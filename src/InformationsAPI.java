import java.util.ArrayList;
import org.bson.Document;

class InformationsAPI {

	/*
	 * Question 1.1
	 */
	private final static String ApiKey = "4c232ae9bf4224b47db636eda0fd2c5e";


	/*
	 * Question 1.3 & Question 1.4
	 */
	Document recupererTitreJSON(String nomArtiste, String titre) {
		HTTPTools tools = new HTTPTools();
		
		String url = "http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key="
				+ ApiKey
				+ "&artist="
				+nomArtiste
				+"&track="
				+titre
				+"&format=json";


		Document docGlobal = Document.parse(tools.sendGet(url));
		Document docTrack = (Document) docGlobal.get("track");
		Document docArtist = (Document) docTrack.get("artist");
		Document docTag = (Document) docTrack.get("toptags");

		String chaine = "{\n"
				+ "\"name\":\""+docTrack.get("name")+"\", \n "
				+ "\"mbid\": \""+docTrack.get("mbid")+"\",\n"
				+ "\"url\": \""+docTrack.get("url")+"\",\n"
				+ "\"duration\": \""+docTrack.get("duration")+"\",\n"
			
					+ "\"artist\": { \n"
						+ "\"name\":\""+docArtist.get("name")+"\", \n "
						+ "\"mbid\": \""+ docArtist.get("mbid")+"\" \n"
					+" }, \n"
						
					+"\"tags\": [ \n";
					
			ArrayList<Document> tags = (ArrayList<Document>) docTag.get("tag");
			for(Document value : tags) {
				chaine = chaine + "\"" + value.get("name")+ "\", \n ";
			}

			chaine = chaine.substring(0,chaine.length()-3) + " \n ] "
				+ "\n}";
			
		return Document.parse(chaine);
		
	}
	

	Document recupererAlbumJSON(String nomArtiste, String nomAlbum) {
		HTTPTools tools = new HTTPTools();
		
		String url = "http://ws.audioscrobbler.com/2.0/?method=album.getInfo&api_key="
				+ ApiKey 
				+ "&artist="
				+nomArtiste
				+"&album="
				+nomAlbum 
				+ "&format=json";
		

		Document docGlobal = Document.parse(tools.sendGet(url));
		Document docAlbum = (Document) docGlobal.get("album");
		Document docWiki = (Document) docAlbum.get("wiki");

		String date = (String) docWiki.get("published");
		String[] splitDate = date.split(" ");
		
		String chaine = "{\n" 
				+ "\"name\":\""+docAlbum.get("name")+"\", \n "
				+ "\"artist\": \""+docAlbum.get("artist")+"\",\n"
				+ "\"mbid\": \""+docAlbum.get("mbid")+"\",\n"
				+ "\"url\": \""+docAlbum.get("url")+"\",\n"
				+ "\"released\": \""+splitDate[2]+"\",\n"
				+"\"images\": [ \n";
		
		//Ajout des images
		ArrayList<Document> images = (ArrayList<Document>) docAlbum.get("image");
		for(Document value : images) {
			chaine = chaine + "{\n" 
					+ "\"size\":\""+value.get("size")+"\", \n "
					+ "\"imageurl\": \""+value.get("#text")+"\"\n"
					+ "} , \n";

		}

		chaine = chaine.substring(0,chaine.length()-3) + " \n ], \n "
				+ "\"listeners\":\""+docAlbum.get("listeners")+"\", \n "
				+ "\"playcounts\": \""+docAlbum.get("playcounts")+"\",\n"
				+"\"tracks\": [ \n";
		
		Document docTracks = (Document) docAlbum.get("tracks");
		ArrayList<Document>  listeTrack = (ArrayList<Document>) docTracks.get("track");
		
		for(Document value : listeTrack) {
	
			Document doc = (Document) value.get("@attr");
			chaine = chaine + "{ \n"
					+ "\"name\":\""+value.get("name")+"\", \n "
					+ "\"tracknumber\":\""+doc.get("rank")+"\", \n "
					+ "\"mbid\":\""+value.get("mbid")+"\" \n },\n";
		}
		
		
		chaine = chaine.substring(0,chaine.length()-2) + " \n ] "
				+ "\n}";
			
		
		return Document.parse(chaine);
		
	}
}
