import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import java.io.IOException;

public class Connection {
	
	private MongoDatabase database;

	/*
	 * Question 2.1
	 */
	public Connection() {
		MongoClient mongoClient = new MongoClient();
		this.database = mongoClient.getDatabase("musicdb");
	}

	/*
	 * Question 2.2
	 */
	public void ajouterDocumentCollection(Document doc, String collection) {
		MongoCollection<Document> mdbCol = this.database.getCollection(collection);
		mdbCol.insertOne(doc);
	}

	/*
	 * Question 2.3
	 */
	public void supprimerMbidCollection(String mbid, String collection) {
		MongoCollection<Document> mdbCol = this.database.getCollection(collection);
		BasicDBObject query = new BasicDBObject();
		query.put("mbid", mbid);
		mdbCol.findOneAndDelete(query);	
	}

	/*
	 * Question 2.4
	 */
	void export(String collection) throws IOException {
		Runtime rt = Runtime.getRuntime();
		Process pr = rt.exec("mongoexport --db musicdb --collection " + collection + " --out " + collection +".json");
	}
	
}
