import org.bson.Document;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        InformationsAPI info = new InformationsAPI();
        Connection mongoDB = new Connection();

        HashMap<String, String> tracksMap = new HashMap<>();
        tracksMap.put("Kanye West", "Stronger");
        tracksMap.put("Pink Floyd", "Time");
        tracksMap.put("Drake", "Over");
        tracksMap.put("Rihanna", "Disturbia");
        tracksMap.put("Nirvana", "Polly");
        tracksMap.put("Cher", "Believe");

        HashMap<String, String> albumsMap = new HashMap<>();
        albumsMap.put("Nirvana", "Nevermind");
        albumsMap.put("Lana Del Rey", "Born to die");
        albumsMap.put("Radiohead", "The Bends");
        albumsMap.put("The Beatles", "Revolver");
        albumsMap.put("Artic Monkeys", "AM");
        albumsMap.put("Kendrick Lamar", "Section.80");

        for(Map.Entry<String, String> entry : tracksMap.entrySet()) {
           Document document = info.recupererTitreJSON(entry.getKey(), entry.getValue());
           mongoDB.ajouterDocumentCollection(document, "tracks");
        }

        for(Map.Entry<String, String> entry : albumsMap.entrySet()) {
            Document document = info.recupererAlbumJSON(entry.getKey(), entry.getValue());
            mongoDB.ajouterDocumentCollection(document, "albums");
        }

        try {
            mongoDB.export("albums");
            mongoDB.export("tracks");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
